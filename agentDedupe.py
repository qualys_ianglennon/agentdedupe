#!/usr/bin/env python3.5
from time import sleep
import requests
import copy
import xml.etree.ElementTree as ET

# Generate a Service Request XML defining the starting point and number of records returned
# Used as the body of the API call in the requestAgentList function
def generateServiceRequest(offset, limit):
    document = ET.Element('ServiceRequest')
    filter = ET.SubElement(document,'filters')
    criteria = ET.SubElement(filter, 'Criteria', {'field': 'trackingMethod', 'operator':'EQUALS'})
    criteria.text = 'QAGENT'
    prefs = ET.SubElement(document, 'preferences')
    start = ET.SubElement(prefs, 'startFromOffset')
    start.text = str(offset)
    results = ET.SubElement(prefs, 'limitResults')
    results.text = str(limit)
    xmlString = ET.tostring(document, encoding='UTF-8', method='xml').decode()
    return xmlString

# Make the API call to request the next batch of records
# Used by the compileAssetList function
def requestAgentList(creds, pod,sr):
    url = pod + '/qps/rest/2.0/search/am/hostasset'
    headers = {'X-Requested-With': 'python3/httplib', \
               'Content-type': 'text/xml',\
               'Content-length': '%s' % (len(sr)) \
               }
    s = requests.Session()
    s.auth = creds
    s.headers.update({'X-Requested-With': 'python3/requests'})

    req = requests.Request('POST',url,headers=headers,data={},auth=s.auth)
    prepped = s.prepare_request(req)
    prepped.body = sr

    # This exception handling is required due to some issues when making API calls
    # If an API call fails, this handling with catch it and return None to the calling function
    try:
        resp = s.send(prepped)
        return resp
    except:
        return None

# Compare two assets to determine if they are duplicates
def compareAssets(assetA, assetB):
    matchScore = 0
    dnsA = None
    dnsB = None
    fqdnA = None
    fqdnB = None
    statusA = None
    statusB = None
    checkInA = None
    checkInB = None
    scoreA = 0
    scoreB = 0

    nameA = assetA.find('name').text
    nameB = assetB.find('name').text
    dnsAelem = assetA.find('dnsHostName')
    if dnsAelem != None:
        dnsA = dnsAelem.text
    dnsBelem = assetB.find('dnsHostName')
    if dnsBelem != None:
        dnsB = dnsBelem.text
    fqdnAelem = assetA.find('fqdn')
    if fqdnAelem != None:
        fqdnA = fqdnAelem.text
    fqdnBelem = assetB.find('fqdn')
    if fqdnBelem != None:
        fqdnB = fqdnBelem.text
    agentInfoAelem = assetA.find('agentInfo')
    agentInfoBelem = assetB.find('agentInfo')
    if agentInfoAelem != None:
        statusA = agentInfoAelem.find('status').text
    if agentInfoBelem != None:
        statusB = agentInfoBelem.find('status').text

    if nameA == nameB:
        matchScore = matchScore + 10
        if dnsA != None and dnsB != None and dnsA == dnsB:
            matchScore = matchScore + 10
        else:
            matchScore = matchScore - 10
        if fqdnA != None and fqdnB != None and fqdnA == fqdnB:
            matchScore = matchScore + 10
        else:
            matchScore = matchScore - 10
        if matchScore >= 10:
            if statusA == "STATUS_INACTIVE":
                scoreA = matchScore + 100
            if statusB == "STATUS_INACTIVE":
                scoreB = matchScore + 100

    return (scoreA, statusA, scoreB, statusB)


# Process the list of agents to find duplicate entities
def parseList(agentList):
    duplicates = []
    matches = []
    for agentA in agentList:
        assetIDA = agentA.find('id').text
        for agentB in agentList:
            assetIDB = agentB.find('id').text
            # do not compare the same asset record
            if assetIDA != assetIDB:
                (scoreA, statusA, scoreB, statusB) = compareAssets(agentA, agentB)
                if scoreA > 0:
                    print("Asset %s match with %s" % (assetIDA, assetIDB))
                    matches.append([assetIDA, assetIDB])
                    if statusA == "STATUS_INACTIVE":
                        agentID = agentA.find('agentInfo').find('agentId').text
                        if agentID not in duplicates:
                            duplicates.append(agentID)
                            print("Added %s to duplicates list" % (agentID))
                        else:
                            print("Asset %s already in duplicates list" % (agentID))
                if scoreB > 0:
                    print("Asset %s match with %s" % (assetIDB, assetIDA))
                    matches.append([assetIDB, assetIDA])
                    if statusB == "STATUS_INACTIVE":
                        agentID = agentB.find('agentInfo').find('agentId').text
                        if agentID not in duplicates:
                            duplicates.append(agentID)
                            print("Added %s to duplicates list" % (agentID))
                        else:
                            print("Asset %s already in duplicates list" % (agentID))
    print("Found %s duplicates" % (len(duplicates)))
    return (matches, duplicates)

# Compile a list of all Cloud Agent assets
# Uses repeated calls to requestAgentList and generateServiceRequest to obtain the list in batches
def compileAssetList(agentList,creds):
    records = 100
    offset = 1
    atEnd = False
    while atEnd == False:
        responseText = ""
        print("Getting %s records from %s" % (records, offset))
        sr = generateServiceRequest(offset, records)

        # The requestAgentList function will return None if the API call failed
        # This while loop will ensure that the call completes even in the event of multiple failures
        while responseText == "":
            response = requestAgentList(creds,'https://qualysapi.qualys.eu',sr)
            if response == None:
                print("Connection failed, retrying in 5 seconds")
                sleep(5)
            else:
                responseText = response.text

        agentList = ET.fromstring(responseText)
        count = int(agentList.find('count').text)

        for agent in agentList.find('data').findall('HostAsset'):
            dupe = copy.deepcopy(agent)
            agentTree.append(dupe)

        if count < records:
            print("Completed with %s records" % (offset + count))
            atEnd = True
        else:
            offset = offset + records

    return agentTree

# Uninstall the duplicate agents
# This is a dangerous call and will permanently remove agents from the subscription.  Use with caution.
# The actual API call section is disabled by default.
def uninstallAgents(agentList, creds):
    # Create useable list of agents from input
    agentStr = ""
    for agent in agentList:
        if len(agentStr) == 0:
            agentStr = str(agent)
        else:
            agentStr = agentStr + "," + str(agent)

    # Create the Service Request XML
    sr = ET.Element('ServiceRequest')
    filters = ET.SubElement(sr, 'filters')
    criteria = ET.SubElement(filters, 'Criteria', {'field': 'agentUuid', 'operator': 'IN'})
    criteria.text = agentStr

    print(ET.tostring(sr, encoding='utf-8', method='xml').decode())

    # Create the request session
    url = 'https://qualysapi.qualys.eu/qps/rest/2.0/uninstall/am/asset'
    headers = {'X-Requested-With': 'python3/httplib', \
               'Content-type': 'text/xml',\
               'Content-length': '%s' % (len(sr)) \
               }

    # UNCOMMENT THIS SECTION TO ENABLE API CALL

    #s = requests.Session()
    #s.auth = creds
    #s.headers.update({'X-Requested-With': 'python3/requests'})

    #req = requests.Request('POST',url,headers=headers,data={},auth=s.auth)
    #prepped = s.prepare_request(req)
    #prepped.body = sr
    #try:
    #    resp = s.send(prepped)
    #    return resp
    #except:
    #    return None


# The main body of the script - executed when the script is run
if __name__ == '__main__':
    # Credentials required to access the subscription - must be Manager role with API access
    creds = ('username', 'password')

    # Build the list of agent assets - comment out these two lines if using a locally cached copy
    agentTree = ET.Element('Agents')
    agentTree = compileAssetList(agentTree, creds)

    # Cache a local copy
    outfile = open('agentTree.xml', 'w')
    outfile.write(ET.tostring(agentTree, encoding ='UTF-8', method='xml').decode())
    outfile.close()

    # Get the list of agent assets using the locally cached copy - uncomment these two lines to enable
    # agentTree = ET.parse('agentTree.xml')
    # agentList = agentTree.getroot()

    # Find the duplicates and create two lists
    #     - one containing matching Asset IDs
    #     - one containing duplicate agentUUIDs
    (matches, duplicates) = parseList(agentList)

    # Write the duplicates to a file
    dupefile = open('duplicates.txt', 'w')
    for dupe in duplicates:
        dupefile.write(str(dupe) + "\n")
    dupefile.close()

    # Write the match search strings to a file and format for an AssetView search
    matchfile = open('matches.txt', 'w')
    matchfile.write('Active records shown first\n')
    for match in matches:
        matchstr = "assetId: %s or assetId: %s\n" % (match[0], match[1])
        matchfile.write(matchstr)
    matchfile.close()

    # Uninstall function disabled.  Uncomment here and within the uninstallAgents function to enable
    #uninstallAgents(duplicates, creds)


